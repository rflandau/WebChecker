#!/bin/bash
# Uses javac to compile class files into './classes',
# notes a class path with the Apache Commons library jar
# and accepts all './src/*.java'.
javac -d classes/ -cp lib/commons-cli-1.4.jar src/*.java
