# Flow

WebChecker follows a simple flow, broken up across multiple files and classes for ease-of-use.

1. Generate cli options with Opts.generateOptions(), then parse mains' args for these options.
    1. This is all handled in the Opts class.
    2. parseOpts() also sets global flags maintained by the class Globals, which are not to be changed after this call.
2. Set recently-parsed input and output files.
3. Parse the file, build the list of links to be worked on.
    1. 



# Other Classes and Subroutines

## IOUtils

Hosts static subroutines useful for IO. qpln() is of greatest note.

### qpln()

Short for ''quiet print line', the subroutine is just shorthand for "only print this if the quiet flag was not set".
Occurs across basically every class, despite its simplicity.