import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/* Takes in a list of urls, retrieves their response code and https status,
 * and spits out the responses.
 * By Rory Landau
 * For Nexbooks.org */

// TODO check for BOM headers that fuck up the first line/section?
// TODO Cut down on false positives
// TODO program throws an error if directory is not found for output file.

public class WebChecker {
  public static void main(String[] args) throws IOException{
    //variables
    long stime = System.currentTimeMillis();
    ArrayList<Link> links;//an ArrayList to hold the given links and locations
    Scanner scan = null; //used to read given files
    File inFile; File outFile; //if outfile is null, print to STDOUT
    Options options; CommandLine cmd;

    // build, then parse, args
    options = Opts.generateOptions();
    cmd = Opts.parseOpts(options, args);
    // interrogate args
    Opts.setGlobals(cmd);
    inFile = IOUtils.createInFile(cmd.getOptionValue("i"));
    outFile = IOUtils.createOutFile(cmd.getOptionValue("o"));

    IOUtils.qpln("Starting WebChecker...");

    // parse the file, create list of links
    IOUtils.qpln("Parsing input file...");
    links = Parse.parseFile(inFile);

    // attempt to connect each link
    IOUtils.qpln("Connecting to links...");
    Link.connectLinks(links);

    // write links to outfile
    IOUtils.qpln("Writing to " + cmd.getOptionValue("o") + "...");
    Link.writeLinks(outFile, links);

    // log runtime in seconds
    IOUtils.qpln("Program took " + ((float)(System.currentTimeMillis() - stime)/1000) + " seconds");
  }
}
