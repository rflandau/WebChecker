/* Globals
 * A class for variables that is best accessible to all. */

class Globals {
    static boolean debug = false;
    static boolean all = false;
    static boolean quiet = false;
    static int timeout = 400;
    final static String usage = "WebChecker <FLAGS> -i <FILE> -o <FILE>";
}
