import java.io.File;
import java.io.IOException;

class IOUtils{
  /* qpln
   * Checks Globals.quiet and writes given string if false.
   * Short for QuietPrintln.
   * @params - str: the string to write
   * @returns boolean: whether or not it printed */
  static void qpln(String str){ if(!Globals.quiet)System.out.println(str); }

  /* createInFile
   * Creates and tests the argument-given input file. */
  static File createInFile(String name){
    //variables
    File toReturn = new File(name);

    if(!toReturn.exists()) {
      System.err.println("ERROR: Could not locate file " + name);
      System.exit(-1);
    }
    if(!toReturn.canRead()) {
      System.err.println("ERROR: file " + name + " is not readable.");
      System.exit(-1);
    }
    return toReturn;
  }

  /* createOutFile
   * Creates and tests the argument-given output file.
   * If the given file could not be located, this function creates it.
   * If the output file is not writeable, the whole program quits. */
  static File createOutFile(String name)
          throws IOException, SecurityException{
    File toReturn = new File(name);

    if(!toReturn.createNewFile() && Globals.debug)
      System.out.println(
              "Out file " + name + " could not be found and was created.");
    if(!toReturn.canWrite()) {
      System.err.println("ERROR: file " + name + " is not writable.");
      System.exit(-1);
    }
    return toReturn;
  }
}
