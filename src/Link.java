import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/* Link Class */
public class Link {
  private final int UNTESTED = -100;
  private final int DEAD = -1;

  //variables
  private String url_s = null; //holds unvalidated or broken url
  private URL url = null; //proper urls are saved and not retested.
  private int responseCode = UNTESTED; //the response code the url provides
  String notes = ""; //notes for the link, separated by a "|"
  private String section, subsection;

  //string
  Link(String s, String section, String subsection) {
    this.url_s = s;
    this.section = section;
    this.subsection = subsection;
  }
  //url
  Link(URL u, String section, String subsection) {
    this.url = u;
    this.section = section;
    this.subsection = subsection;
  }


  //functions
  //getters-----------------------------------------------------------------------------------------------------------
  /* Returns the url */
  public URL getURL() {return url;}
  /* Returns the url's response code.*/
  public int getResponseCode() {return responseCode;}
  /* Gets the location of the url */
  public String getSection() {return section;}
  //setters-----------------------------------------------------------------------------------------------------------
  /* Sets the section of the url */
  void setSection(String s) {section = s;}
  /* Sets the subsection of the url */
  void setSubsection(String s) {subsection = s;}
  /* Sets responseCode */
  void setResponseCode(int r){responseCode = r;}
  /* Sets responseCode to DEAD */
  void setDead(){responseCode = DEAD;}
  //------------------------------------------------------------------------------------------------------------------
  /* Appends a given string to the notes variable */
  void addNote(String note) { notes = notes + note + " | "; }


  /* validateURL
   * Tests a string as a URL. Returns null on failure. */
  static URL validateURL(String s){
    URL url;
    try{url = new URL(s);}
    catch(MalformedURLException e){url = null;}
    return url;
  }

  /* connect
   * Attempts to connect to the Link's url.
   * Used to determine the website's status.
   * Adjusts the responseCode and https. */
  void connect() throws ProtocolException {
    // variables
    HttpURLConnection openurl = null;

    // check that URL has been tested
    if(url == null && responseCode != DEAD){
      url = validateURL(url_s);
      //if url is still null, url is broken
      if(url == null) setDead();
    }

    // do not test if already dead
    if(responseCode == DEAD){addNote("Invalid URL"); return;}

    try {
      // openConnection should return an implementation of the abstract HttpURLConnection
      openurl = (HttpURLConnection) url.openConnection();
    } catch (IOException e) {
      if (Globals.debug) System.out.println("caught '" + e.toString() +
        "'.\nSetting Link as dead.");
      responseCode = DEAD;
      addNote("URL could not parsed.");
    }
    if(responseCode != DEAD) { // only runs if the url was properly parsed
      openurl.setRequestMethod("HEAD");
      openurl.setConnectTimeout(Globals.timeout);
      //fetch response code
      try { responseCode = openurl.getResponseCode(); }
      catch (IOException e) {
        responseCode = DEAD;
        addNote("Could not locate");
      }
    }
  }
  /* The toString
   * If the url is null, return the pre-test url.
   * Otherwise, call the url's toString() and return that. */
  public String toString() { return (url == null) ? url_s : url.toString(); }

  public String dump() {
    return " [" + responseCode + "] " + toString() + "\n" +
            "-----Location: " + section + " - " + subsection + "\n" +
            "-----Notes: " + notes;
  }

  //----------------------------------------------------------------------------
  //static functions------------------------------------------------------------
  //----------------------------------------------------------------------------
  /* connectLinks
   * Takes the recently-parsed list of Links and attempt to connect each one.
   * Basically just calls connect() on each link.
   * @params -
   * @returns void */
  static void connectLinks(ArrayList<Link> links) throws ProtocolException {
    for (Link link : links){
      // TODO:
      //  if not debug, use a CR to overwrite previous line.
      //  if debug, use CR + NL
      IOUtils.qpln(link.toString());
      link.connect();
    }
  }

  /* writeLinks
   * Prints links to the given outfile. */
  static void writeLinks(File out, ArrayList<Link> links) throws IOException{
    BufferedWriter br;
    Link curLink;

    //establish output
    br = new BufferedWriter(new FileWriter(out));

    //call toString on each link
    for (Link link : links) {
      curLink = link;
      if (curLink.getResponseCode() != 200 || Globals.all) {
        br.write(curLink.dump());
        br.write("\n\n");
      }
    }
    br.close();
  }
}
