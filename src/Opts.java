import org.apache.commons.cli.*;

class Opts{
  /* generateOptions
   * Builds the cli args using the option builder, adds them to Options,
   * and returns them. */
  static Options generateOptions(){
    Options toReturn = new Options();
    //debug mode
    toReturn.addOption(Option.builder("d")
            .required(false).hasArg(false)
            .longOpt("debug")
            .desc("Enables debug mode, used for debugging")
            .build());
    //print all responses
    toReturn.addOption(Option.builder()
            .required(false).hasArg(false)
            .longOpt("all")
            .desc("Prints all response codes, even 200s")
            .build());
    //suppress output
    toReturn.addOption(Option.builder("q")
            .required(false).hasArg(false)
            .longOpt("quiet")
            .desc("Suppress all operations output")
            .build());
    //help
    toReturn.addOption(Option.builder("h")
            .required(false).hasArg(false)
            .longOpt("help")
            .desc("Howdy")
            .build());
    // url timeout
    toReturn.addOption(Option.builder("t")
            .required(false).hasArg()
            .argName("TIME")
            .longOpt("timeout")
            .desc("Set timeout for links. Default 400")
            .build());
    //specify input file
    toReturn.addOption(Option.builder("i")
            .required().hasArg()
            .argName("FILE")
            .longOpt("in")
            .desc("Required, input file")
            .build());
    //specify out file
    toReturn.addOption(Option.builder("o")
            .required().hasArg()
            .argName("FILE")
            .longOpt("out")
            .desc("Required, output file")
            .build());
    return toReturn;
  }

  /* parseOpts
   * Attempts to parse cli args, killing the program on failure.
   * Returns a CommandLine instance of existing options and values.
   * Note: the help option is checked separately. See Limitations in README */
  static CommandLine parseOpts(Options options, String[] args){
    CommandLineParser parser = new DefaultParser();
    CommandLine cmd = null;
    boolean help = true;

    // check for just help flag
    try{
      cmd = parser.parse(new Options().addOption("h", "help", false, ""), args);
    } catch(ParseException e){help = false;} //if h not first, catch later
    if(help){printHelp(options); System.exit(0);}

    // move on to the full options list
    try{ cmd = parser.parse(options, args); }
    catch(ParseException e){ // parse failed, kill
      System.out.println("usage: " + Globals.usage);
      System.exit(0);
    }

    return cmd;
  }

  /* setGlobals */
  static void setGlobals(CommandLine cmd){
    if(cmd.hasOption("d"))Globals.debug = true; // debug mode
    if(cmd.hasOption("all"))Globals.all = true; // all mode
    if(cmd.hasOption("q"))Globals.quiet = true; //quiet mode
    if(cmd.hasOption("t"))Globals.timeout = Integer.parseInt(cmd.getOptionValue("t")); // timeout
  }

  /* printHelp
   * Spits out the apache-generated help and usage statement. */
  static void printHelp(Options options){
    HelpFormatter hForm = new HelpFormatter();
    //extra text
    String header = //  -----
            "This program takes in any number of urls and pings them, "+
                    "printing non-200 response codes.\n\n"+
                    "Options:\n";
    String footer = "\n" +
            "Source code available at gitlab.com/rflandau/WebChecker";

    //print
    hForm.printHelp(Globals.usage, header, options, footer, false);
  }
}
