import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.net.URL;

enum LineType{EMPTY, SECTION, SUBSECTION, URL, MALFORMED}

public class Parse{

	/* parseFile()
	 * Takes a properly-formatted txt file and converts it to Links,
	 * returning an ArrayList of Link objects.
	 * Improper formatting will cause some wonky behavior.
	 * See ll_template.txt for the proper formatting.
	 * @params -
	 * @returns an arraylist of Links*/
	static ArrayList<Link> parseFile(File inFile){
		ArrayList<Link> toReturn = new ArrayList<>();
		Scanner sc = null;

		// prep scanner
		try{
			sc = new Scanner(inFile);
		}catch(FileNotFoundException e){
			// inFile should already have been checked.
			// if an error pops up here, the infile has disappeared.
			System.err.println("Input file lost.\n" + e);
			System.exit(-1);
		}

		// read text file
		//	sections and subsections are maintained until another instance pops up.
		//	A new section replaces current section and voids subsection.
		//	A new subsection replaced current section.
		//  Both URL and MALFORMED types add a new link using the current sub/section from previous iterations.
		//		MALFORMED is also preemptively marked as dead.
		String section = null;
		String subsection = null;
		while(sc.hasNextLine()){
			String line = sc.nextLine().trim();
			LineType lt = getLineType(line); // check line type
			switch(lt){
				case EMPTY:
					break; // just consume
				case SECTION:
					section = line.substring(1);
					break;
				case SUBSECTION:
					subsection = line.substring(2);
					break;
				case URL:
					URL u = Link.validateURL(line); //redundant.
					toReturn.add(new Link(u, section, subsection));
					break;
				case MALFORMED:
					Link l = new Link(line, section, subsection);
					l.setDead();
					toReturn.add(l);
					break;
			}
		}
		sc.close();
		return toReturn;
	}

	/* getLineType
	 * Tests line for its type by checking prefix and validity as a url.
	 * Stumbles over text editor prepends and I do not know why.
	 * May not be edge-case tested. Don't remember. */
	static LineType getLineType(String line){
		LineType lt;
		if(line.equals("")) lt = LineType.EMPTY;
		else{
			if(line.charAt(0) == '#'){
				if(line.charAt(1) == '#') lt = LineType.SUBSECTION;
				else lt = LineType.SECTION;
			}else{ // validateURL return null on creation error, hence malformed
				if(Link.validateURL(line) != null) lt = LineType.URL;
				else lt = LineType.MALFORMED;
			}
		}
		if(Globals.debug) System.out.println(lt.toString()+": "+line);
		return lt;
	}
}
