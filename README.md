# WebChecker
Determines the up-status of websites, provided in a list in a text file.

## Installation
Use ./compile to compile with the correct class path.
Use ./run to run the example.
See *Perl Script* below on how to run multiple scripts.


## Usage
`WebChecker <FLAGS> -i <arg> -o <arg>`
### Options
   --all            Prints all response codes, even 200s

-d,--debug          Enables debug mode, used for debugging

-h,--help           Print help message

-i,--in FILE        Required, input file

-o,--out FILE       Required, output file

-t,--timeout TIME   Set timeout for links. Default 400


## TODO
-   [ ] Integrate youtube/vimeo/\[other video site] APIs

-   [ ] Add flag to filter 429s

-   [ ] Allow the program to create output directory if not found

-   [ ] Thread each links to pipeline (Measure, may not be worth thead overhead. A Cons/Disp buffer may be more useful)

-   [ ] Finish DESIGN doc

-   [ ] Figure out how tf to properly create a package-able JVM with jlink [???](https:www.devdungeon.com/content/how-create-java-runtime-images-jlink)

-   [ ] Fix varsitytutor.com always returning a 504

-   [ ] When printing normal operation output to CLI, overwrite existing line instead of continuing to next line. Add a no-overwrite flag.

## Perl Script
The included perl script is a simple script to run all files matching `ll_*.txt` in given directory and print their results to `out/[filename].out`.
Call it with no arguments to print help.


## Capabilities and Limitations
### Common Errors
*   Invalid URL

The URL was treated and parsed as a URL, but failed to return anything. Check for spelling errors.
*   URL could not be parsed

The URL is malformed and does not even resemble a URL. Check formatting. Did you include protocol (http/s)?
*   First section being read as malformed url

Honestly, I'm not 100% sure why this happens. Retype the line in Nano or some other ultra-basic editor. I *think* the scanner is stumbling over a BOM header (hence why trim misses it), but I really do not know.

*   429 errors (most notably from youtube)

This is most likely because youtube is rejecting the pings due to volume. Until a youtube API is integrated, this is a fact of life.

*    Valid sites return 504

This is always the case at Varsitytutors.com. I don't currently have a solution, but it has a TODO item.
### Help Overhead
*   There is a redundancy in the way the help flag is checked. Apache Commons CLI does not have a way to ignore required flags if the help flag is detected, meaning we have to parse for help, and, if not found, parse again. Minimal overhead, but still a weakness.


## Structure
The heavy lifting is done by WebChecker, of course, but URLs and their status are held in the Links class for ease-of-use.

### Globals
A small number of variables are held in the Globals class, as it is easiest to allow everything access to the variables than to pass them between every function.
Debug, allmode, timeout, and the 'usage' string are all held in Globals.
