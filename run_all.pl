#!/bin/perl
my $help = << 'end_help';
A script to run all link lists.
Usage:
  run_all.pl [jar] [input dir] [output dir]
end_help

use warnings;
use strict;

##VARIABLES____
my ($a_c, $jar, $in, $out, $outfile);
my @files;

##ARGS_____
$a_c = @ARGV;
die $help . "\n" unless $a_c == 3; #check arg count
$jar = $ARGV[0]; $in = $ARGV[1]; $out = $ARGV[2];
$out = $out . "/" if ((substr $out, -1) ne "/"); #check $out has directory slash

##FILES_____
@files = <$in/*>;
foreach my $file (@files){ #loop through all files in input directory
  if ($file =~ /ll_\S*[.]/ && -f $file){ #check if file and contains "ll_*.txt"
    $outfile = $out . $& . "out"; #'$&' contains matched string of last regex
    my $call = "java -jar " . $jar . " -q -i " . $file . " -o " . $outfile;
    print($call . "\n");
    system($call);
  }

}

exit 0;
